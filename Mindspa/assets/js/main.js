(function ($) {
    back_to_top();
	moblie_bar();
    stuck_header();
    main_slider()
    stt_number_our_app();
    product_slider_1();
    product_slider_2();
})(jQuery);


window.onload = function () {
              
};

$(document).ready(function () {		
			
});  



function back_to_top() {
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 500,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');
	//hide or show the "back to top" link
	$(window).scroll(function () {
		($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if ($(this).scrollTop() > offset_opacity) {
			$back_to_top.addClass('cd-fade-out');
		}
	});
	//smooth scroll to top
	$back_to_top.on('click', function (event) {
		    event.preventDefault();
            $('body,html').animate({
                scrollTop: 0,
            }, scroll_top_duration
		);
	});
}


function moblie_bar() {
    var $main_nav = $('#main-nav');
    var $toggle = $('.toggle');

    var defaultData = {
        maxWidth: false,
        customToggle: $toggle,
        // navTitle: 'All Categories',
        levelTitles: true,
        pushContent: '#container'
    };

    // add new items to original nav
    $main_nav.find('li.add').children('a').on('click', function() {
        var $this = $(this);
        var $li = $this.parent();
        var items = eval('(' + $this.attr('data-add') + ')');

        $li.before('<li class="new"><a>' + items[0] + '</a></li>');

        items.shift();

        if (!items.length) {
            $li.remove();
        } else {
            $this.attr('data-add', JSON.stringify(items));
        }

        Nav.update(true);
    });

    // call our plugin
    var Nav = $main_nav.hcOffcanvasNav(defaultData);

    // demo settings update

    const update = (settings) => {
        if (Nav.isOpen()) {
            Nav.on('close.once', function() {
                Nav.update(settings);
                Nav.open();
            });

            Nav.close();
        } else {
            Nav.update(settings);
        }
    };

    $('.actions').find('a').on('click', function(e) {
        e.preventDefault();

        var $this = $(this).addClass('active');
        var $siblings = $this.parent().siblings().children('a').removeClass('active');
        var settings = eval('(' + $this.data('demo') + ')');

        update(settings);
    });

    $('.actions').find('input').on('change', function() {
        var $this = $(this);
        var settings = eval('(' + $this.data('demo') + ')');

        if ($this.is(':checked')) {
            update(settings);
        } else {
            var removeData = {};
            $.each(settings, function(index, value) {
                removeData[index] = false;
            });

            update(removeData);
        }
    });
}


function main_slider(){
    new KeenSlider("#main-slider", { 
        loop: true,
        created: function (instance) {
            var dots_wrapper = document.querySelector(".main-slider #dots");
            var slides = document.querySelectorAll(".main-slider .keen-slider__slide");
            slides.forEach(function (t, idx) {
            var dot = document.createElement("button");
            dot.classList.add("dot");
            dots_wrapper.appendChild(dot);
            dot.addEventListener("click", function () {
                instance.moveToSlide(idx);
            });
            });
            updateClasses(instance);
        },
        slideChanged(instance) {
            updateClasses(instance);
        }
    })

    function updateClasses(instance) {
        var slide = instance.details().relativeSlide;
      
        var dots = document.querySelectorAll(".main-slider .dot");
        dots.forEach(function (dot, idx) {
          idx === slide
            ? dot.classList.add("dot--active")
            : dot.classList.remove("dot--active");
        });
    }
}

function stt_number_our_app(){
    var li = document.querySelectorAll(".our-app__box--list li");
    if(li == null){
        return 0;
    }
    else{
        var count = 0;
        for(var i = 0; i < li.length; i++){
            count ++;
            li[i].setAttribute('data-number', count);
        }
    }
}

function product_slider_1(){
    var slider_product = new KeenSlider("#home-blog_slider--1", {
        loop: true,
        slidesPerView: 3,
        spacing: 30,
        breakpoints: {

          '(min-width: 300px)': {
            slidesPerView: 1,
          },

          '(min-width: 767px)': {
            slidesPerView: 2,
          },

          '(min-width: 1100px)': {
            slidesPerView: 3,
          },
        },
        created: function (instance) {
            var arr_left = document.querySelector(".our-blogs #arrow-left");

            if(arr_left == null){
                return 0;
            }
            else{
                arr_left.addEventListener("click", function () {
                    instance.prev();
                })
            }

            var arr_right = document.querySelector(".our-blogs #arrow-right");

            if(arr_right == null){
                return 0;
            }
            else{
                arr_right.addEventListener("click", function () {
                    instance.next()
                })
            }
   
            var dots_wrapper = document.querySelector(".our-blogs #dots")
            var slides = document.querySelectorAll(".our-blogs .keen-slider__slide")
            slides.forEach(function (t, idx) {
              var dot = document.createElement("button")
              dot.classList.add("dot")
              dots_wrapper.appendChild(dot)
              dot.addEventListener("click", function () {
                instance.moveToSlide(idx)
              })
            })
            updateClasses(instance)
        },
        slideChanged(instance) {
            updateClasses(instance)
        },

    })

    function updateClasses(instance) {
        var slide = instance.details().relativeSlide
        var arrowLeft = document.querySelector(".our-blogs #arrow-left")
        var arrowRight = document.querySelector(".our-blogs #arrow-right")
        slide === 0
          ? arrowLeft.classList.add("arrow--disabled")
          : arrowLeft.classList.remove("arrow--disabled")
        slide === instance.details().size - 1
          ? arrowRight.classList.add("arrow--disabled")
          : arrowRight.classList.remove("arrow--disabled")
      
        var dots = document.querySelectorAll(".our-blogs .dot")
        dots.forEach(function (dot, idx) {
          idx === slide
            ? dot.classList.add("dot--active")
            : dot.classList.remove("dot--active")
        })
    }

    var dot_btn = document.querySelectorAll(".our-blogs #dots .dot");

    if(dot_btn == null){
        return 0;
    }
    else{
        var count = 0;

        for(var i = 0; i< dot_btn.length; i++){
            count ++;
            dot_btn[i].innerHTML = count + "/";
        }

        var dots_wrapper = document.querySelector(".our-blogs #dots");
        if(dots_wrapper == null){
            return 0;
        }
        else{
            dots_wrapper.setAttribute('data-number', dot_btn.length);
        }

        
    }

}

function product_slider_2(){
    var slider_product = new KeenSlider("#home-blog_slider--2", {
        loop: true,
        slidesPerView: 3,
        spacing: 30,
        breakpoints: {

          '(min-width: 300px)': {
            slidesPerView: 1,
          },

          '(min-width: 767px)': {
            slidesPerView: 2,
          },

          '(min-width: 1100px)': {
            slidesPerView: 2,
          },
        },
        created: function (instance) {
            var arr_left = document.querySelector(".customer-comments #arrow-left");

            if(arr_left == null){
                return 0;
            }
            else{
                arr_left.addEventListener("click", function () {
                    instance.prev();
                })
            }

            var arr_right = document.querySelector(".customer-comments #arrow-right");

            if(arr_right == null){
                return 0;
            }
            else{
                arr_right.addEventListener("click", function () {
                    instance.next()
                })
            }
   
            var dots_wrapper = document.querySelector(".customer-comments #dots")
            var slides = document.querySelectorAll(".customer-comments .keen-slider__slide")
            slides.forEach(function (t, idx) {
              var dot = document.createElement("button")
              dot.classList.add("dot")
              dots_wrapper.appendChild(dot)
              dot.addEventListener("click", function () {
                instance.moveToSlide(idx)
              })
            })
            updateClasses(instance)
        },
        slideChanged(instance) {
            updateClasses(instance)
        },

    })

    function updateClasses(instance) {
        var slide = instance.details().relativeSlide
        var arrowLeft = document.querySelector(".customer-comments #arrow-left")
        var arrowRight = document.querySelector(".customer-comments #arrow-right")
        slide === 0
          ? arrowLeft.classList.add("arrow--disabled")
          : arrowLeft.classList.remove("arrow--disabled")
        slide === instance.details().size - 1
          ? arrowRight.classList.add("arrow--disabled")
          : arrowRight.classList.remove("arrow--disabled")
      
        var dots = document.querySelectorAll(".customer-comments .dot")
        dots.forEach(function (dot, idx) {
          idx === slide
            ? dot.classList.add("dot--active")
            : dot.classList.remove("dot--active")
        })
    }

    var dot_btn = document.querySelectorAll(".customer-comments #dots .dot");

    if(dot_btn == 'null'){
        return 0;
    }
    else{
        var count = 0;

        for(var i = 0; i< dot_btn.length; i++){
            count ++;
            dot_btn[i].innerHTML = count + "/";
        }

        var dots_wrapper = document.querySelector(".customer-comments #dots");

        if(dots_wrapper == null){
            return 0;
        }
        else{
            dots_wrapper.setAttribute('data-number', dot_btn.length);
        }
    }

}


function stuck_header(){
   
    var header = document.querySelector(".header-main");

    if(header == null){
        return 0;
    }
    else{
        var check = true;
        window.addEventListener("scroll", function(){
            if(window.pageYOffset > 50){
                if(check == true){
                    header.classList.add("stuck");
                    check = false;
                }
            }
            else{
                if(check == false){
                    header.classList.remove("stuck");
                    check = true;
                }
            }
        })
    }

    
}